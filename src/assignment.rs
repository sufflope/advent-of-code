use std::ops::Range;

use crate::ParseError;

struct Assignment(Range<usize>);

impl Assignment {
    fn overlaps(&self, other: &Assignment) -> bool {
        other.0.start <= self.0.end && other.0.end >= self.0.start
    }
}

impl TryFrom<&str> for Assignment {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let bounds = value.split('-').collect::<Vec<_>>();
        match bounds.as_slice() {
            &[start, end] => {
                fn parse(value: &str) -> Result<usize, ParseError> {
                    value.parse::<usize>().map_err(|_| ParseError {
                        kind: "bound".to_string(),
                        spec: value.to_string(),
                    })
                }
                let start = parse(start)?;
                let end = parse(end)?;
                Ok(Assignment(Range { start, end }))
            }
            _ => Err(ParseError {
                kind: "assignment".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

pub struct Pair(Assignment, Assignment);

impl Pair {
    pub fn parse_vec(value: &str) -> Result<Vec<Pair>, ParseError> {
        value.lines().map(|line| line.try_into()).collect()
    }

    pub fn overlaps(&self) -> bool {
        self.0.overlaps(&self.1)
    }
}

impl TryFrom<&str> for Pair {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let assignments = value.split(',').collect::<Vec<_>>();
        match assignments.as_slice() {
            &[first, second] => {
                let first = first.try_into()?;
                let second = second.try_into()?;
                Ok(Pair(first, second))
            }
            _ => Err(ParseError {
                kind: "assignment pair".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_overlaps_example() -> anyhow::Result<()> {
        let pairs = Pair::parse_vec(
            "2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8",
        )?;
        assert_eq!(pairs.iter().filter(|pair| pair.overlaps()).count(), 4);
        Ok(())
    }

    #[test]
    fn test_overlaps_input() -> anyhow::Result<()> {
        let pairs = Pair::parse_vec(include_str!("../resources/assignments.txt"))?;
        assert_eq!(pairs.iter().filter(|pair| pair.overlaps()).count(), 952);
        Ok(())
    }
}
