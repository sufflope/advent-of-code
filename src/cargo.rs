use itertools::Itertools;
use lazy_static::lazy_static;
use regex::Regex;

use crate::ParseError;

struct Cargo(Vec<Vec<Crate>>);

impl TryFrom<&str> for Cargo {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut lines = value.lines().rev();
        let size = lines
            .next()
            .ok_or_else(|| ParseError {
                kind: "cargo".to_string(),
                spec: value.to_string(),
            })?
            .split(' ')
            .last()
            .ok_or_else(|| ParseError {
                kind: "cargo indices".to_string(),
                spec: value.to_string(),
            })?
            .parse::<usize>()
            .map_err(|_| ParseError {
                kind: "index".to_string(),
                spec: value.to_string(),
            })?;
        let mut stacks = vec![];
        stacks.resize_with(size, Vec::new);
        for line in lines {
            for (i, chunk) in line.chars().chunks(4).into_iter().enumerate() {
                let chars = chunk.into_iter().collect::<Vec<_>>();
                let c = chars[1];
                if c.is_ascii_alphabetic() {
                    stacks[i].push(Crate(c))
                } else if c != ' ' {
                    return Err(ParseError {
                        kind: "crate".to_string(),
                        spec: String::from_iter(chars),
                    });
                }
            }
        }
        Ok(Cargo(stacks))
    }
}

struct Crate(char);

struct Step {
    count: usize,
    source: usize,
    destination: usize,
}

impl TryFrom<&str> for Step {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"move (\d+) from (\d+) to (\d+)").unwrap();
        }
        match RE.captures_iter(value).next() {
            Some(step) => {
                fn parse(value: &str) -> Result<usize, ParseError> {
                    value.parse().map_err(|_| ParseError {
                        kind: "step usize".to_string(),
                        spec: value.to_string(),
                    })
                }
                Ok(Step {
                    count: parse(&step[1])?,
                    source: parse(&step[2])?,
                    destination: parse(&step[3])?,
                })
            }
            None => Err(ParseError {
                kind: "step".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

pub struct Operator {
    cargo: Cargo,
    instructions: Vec<Step>,
}

impl Operator {
    pub fn operate(&mut self) -> String {
        for step in &self.instructions {
            let stacks = &mut self.cargo.0;
            let source = &mut stacks[step.source - 1];
            let crates = source
                .drain((source.len() - step.count)..)
                .collect::<Vec<_>>();
            stacks[step.destination - 1].extend(crates);
        }
        self.instructions.clear();
        String::from_iter(self.cargo.0.iter().map(|stack| stack.last().unwrap().0))
    }
}

impl TryFrom<&str> for Operator {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let (cargo, instructions) = value.split_once("\n\n").ok_or_else(|| ParseError {
            kind: "operator".to_string(),
            spec: value.to_string(),
        })?;
        let cargo = Cargo::try_from(cargo)?;
        let instructions = instructions
            .lines()
            .map(Step::try_from)
            .collect::<Result<_, _>>()?;
        Ok(Operator {
            cargo,
            instructions,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_operate_example() -> anyhow::Result<()> {
        let mut operator = Operator::try_from(
            "    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2",
        )?;
        assert_eq!(&operator.operate(), "MCD");
        Ok(())
    }

    #[test]
    fn test_operate_input() -> anyhow::Result<()> {
        let mut operator = Operator::try_from(include_str!("../resources/cargo.txt"))?;
        assert_eq!(&operator.operate(), "LBBVJBRMH");
        Ok(())
    }
}
