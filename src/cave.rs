use std::cmp::{max, min};
use std::collections::HashSet;

use combine::parser::char::{char as ch, string};
use combine::{many1, parser::char::digit, sep_by1, EasyParser, Parser, Stream};
use itertools::Itertools;

use crate::{ParseError, Position};

#[derive(Default)]
pub struct Cave {
    occupied: HashSet<Position>,
    floor: usize,
}

impl Cave {
    pub fn fill(&mut self) -> usize {
        fn mov(cave: &Cave, sand: &mut Position) -> bool {
            if sand.y == cave.floor - 1 {
                false
            } else if !cave.occupied.contains(&Position {
                x: sand.x,
                y: sand.y + 1,
            }) {
                sand.y += 1;
                true
            } else if !cave.occupied.contains(&Position {
                x: sand.x - 1,
                y: sand.y + 1,
            }) {
                sand.x -= 1;
                sand.y += 1;
                true
            } else if !cave.occupied.contains(&Position {
                x: sand.x + 1,
                y: sand.y + 1,
            }) {
                sand.x += 1;
                sand.y += 1;
                true
            } else {
                false
            }
        }
        let mut count = 0;
        loop {
            let mut sand = Position { x: 500, y: 0 };
            while mov(self, &mut sand) {}
            let last = sand == Position { x: 500, y: 0 };
            self.occupied.insert(sand);
            count += 1;
            if last {
                return count;
            }
        }
    }
}

impl TryFrom<&str> for Cave {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut cave = Cave::default();
        for line in value.lines() {
            let (s, _) = structure().easy_parse(line).map_err(|_| ParseError {
                kind: "structure".to_string(),
                spec: line.to_string(),
            })?;
            for (start, end) in s.iter().tuple_windows::<(&Position, &Position)>() {
                for x in min(start.x, end.x)..=max(start.x, end.x) {
                    for y in min(start.y, end.y)..=max(start.y, end.y) {
                        cave.occupied.insert(Position { x, y });
                    }
                }
            }
        }
        cave.floor = cave.occupied.iter().map(|p| p.y).max().unwrap() + 2;
        Ok(cave)
    }
}

fn position<Input>() -> impl Parser<Input, Output = Position>
where
    Input: Stream<Token = char>,
{
    (many1(digit()), ch(','), many1(digit())).map(|(x, _, y): (String, _, String)| Position {
        x: x.parse::<usize>().unwrap(),
        y: y.parse::<usize>().unwrap(),
    })
}

fn structure<Input>() -> impl Parser<Input, Output = Vec<Position>>
where
    Input: Stream<Token = char>,
{
    sep_by1(position(), string(" -> "))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sand_example() -> anyhow::Result<()> {
        let mut cave = Cave::try_from(
            "498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9",
        )?;
        let count = cave.fill();
        assert_eq!(count, 93);
        Ok(())
    }

    #[test]
    fn test_sand_input() -> anyhow::Result<()> {
        let mut cave = Cave::try_from(include_str!("../resources/cave.txt"))?;
        let count = cave.fill();
        assert_eq!(count, 30157);
        Ok(())
    }
}
