use std::fmt::{Display, Formatter};

use derive_more::{Deref, DerefMut};

use crate::ParseError;

pub struct Cpu {
    x: isize,
    cycle: usize,
    state: State,
}

impl Default for Cpu {
    fn default() -> Self {
        Self {
            x: 1,
            cycle: 0,
            state: State::Idle,
        }
    }
}

impl Cpu {
    fn tick(&mut self) {
        match self.state {
            State::Idle => {}
            State::Executing {
                ref instruction,
                ref mut remaining,
            } => {
                if remaining == &0 {
                    if let Instruction::Addx(value) = instruction {
                        self.x += value;
                    }
                    self.state = State::Idle;
                } else {
                    *remaining -= 1;
                }
            }
        }
        self.cycle += 1;
    }

    pub fn run(&mut self, program: &str) -> Result<Crt, ParseError> {
        let instructions = program
            .lines()
            .map(Instruction::try_from)
            .collect::<Result<Vec<_>, _>>()?;
        let mut instructions = instructions.into_iter();
        let mut crt = Crt::default();
        loop {
            self.tick();
            if self.state == State::Idle {
                if let Some(instruction) = instructions.next() {
                    let length = instruction.length();
                    self.state = State::Executing {
                        instruction,
                        remaining: length - 1,
                    }
                } else {
                    break;
                }
            }
            let x = (self.cycle - 1) % X;
            let y = (self.cycle - 1) / 40;
            crt[y][x] = if (x as isize - self.x).abs() <= 1 {
                '#'
            } else {
                '.'
            }
        }
        Ok(crt)
    }
}

#[derive(PartialEq)]
enum State {
    Idle,
    Executing {
        instruction: Instruction,
        remaining: usize,
    },
}

#[derive(PartialEq)]
enum Instruction {
    Addx(isize),
    Noop,
}

impl Instruction {
    fn length(&self) -> usize {
        match self {
            Instruction::Addx(_) => 2,
            Instruction::Noop => 1,
        }
    }
}

impl TryFrom<&str> for Instruction {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "noop" => Ok(Instruction::Noop),
            addx if addx.starts_with("addx ") => {
                let val = addx.strip_prefix("addx ").unwrap();
                let val = val.parse().map_err(|_| ParseError {
                    kind: "addx argument".to_string(),
                    spec: val.to_string(),
                })?;
                Ok(Instruction::Addx(val))
            }
            _ => Err(ParseError {
                kind: "instruction".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

const X: usize = 40;
const Y: usize = 6;

#[derive(Deref, DerefMut)]
pub struct Crt([[char; X]; Y]);

impl Default for Crt {
    fn default() -> Self {
        Self([['\0'; X]; Y])
    }
}

impl Display for Crt {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        writeln!(f)?;
        for y in 0..Y {
            for x in 0..X {
                write!(f, "{}", self[y][x])?
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_run_example() -> anyhow::Result<()> {
        let mut cpu = Cpu::default();
        let crt = cpu.run(
            "addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop
",
        )?;
        assert_eq!(
            crt.to_string(),
            "
##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....
"
        );
        Ok(())
    }

    #[test]
    fn test_run_input() -> anyhow::Result<()> {
        let mut cpu = Cpu::default();
        let crt = cpu.run(include_str!("../resources/instructions.txt"))?;
        assert_eq!(
            crt.to_string(),
            "
###..#..#.###....##.###..###..#.....##..
#..#.#.#..#..#....#.#..#.#..#.#....#..#.
#..#.##...#..#....#.###..#..#.#....#..#.
###..#.#..###.....#.#..#.###..#....####.
#.#..#.#..#....#..#.#..#.#....#....#..#.
#..#.#..#.#.....##..###..#....####.#..#.
"
        );
        Ok(())
    }
}
