use std::cmp::Ordering;
use std::collections::HashMap;
use std::ops::Sub;
use std::str::Chars;

use combine::{
    attempt, choice, many1, parser,
    parser::char::{char as ch, digit},
    sep_by, EasyParser, Parser, Stream,
};
use itertools::Itertools;
use rayon::prelude::*;

use crate::{ParseError, Position};

const PREAMBLE_LEN: usize = 14;

enum State {
    Initializing(Vec<char>),
    Initialized,
}

impl Default for State {
    fn default() -> Self {
        State::Initializing(vec![])
    }
}

pub struct Device<T: Iterator<Item = char>> {
    input: T,
    state: State,
}

impl<T: Iterator<Item = char>> Device<T> {
    pub fn initialize(&mut self) -> Result<usize, String> {
        match &mut self.state {
            State::Initializing(preamble) => {
                for char in self.input.by_ref() {
                    preamble.push(char);
                    if preamble.len() >= PREAMBLE_LEN
                        && preamble[(preamble.len() - PREAMBLE_LEN)..]
                            .iter()
                            .unique()
                            .count()
                            == PREAMBLE_LEN
                    {
                        break;
                    }
                }
                let length = preamble.len();
                self.state = State::Initialized;
                Ok(length)
            }
            _ => Err("Trying to initialize device a second time".to_string()),
        }
    }
}

impl<'a> From<&'a str> for Device<Chars<'a>> {
    fn from(value: &'a str) -> Self {
        Device {
            input: value.chars(),
            state: State::default(),
        }
    }
}

struct Elevation(char);

impl Sub for &Elevation {
    type Output = i8;

    fn sub(self, rhs: Self) -> Self::Output {
        self.0 as i8 - rhs.0 as i8
    }
}

impl TryFrom<char> for Elevation {
    type Error = ParseError;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        if ('a'..='z').contains(&value) {
            Ok(Elevation(value))
        } else {
            match value {
                'S' => Ok(Elevation('a')),
                'E' => Ok(Elevation('z')),
                _ => Err(ParseError {
                    kind: "elevation".to_string(),
                    spec: value.to_string(),
                }),
            }
        }
    }
}

pub struct Heightmap {
    elevations: Vec<Vec<Elevation>>,
    #[allow(dead_code)]
    start: Position,
    goal: Position,
}

impl Heightmap {
    pub fn path(&self) -> usize {
        fn consider(map: &Heightmap, start: &Position) -> Option<usize> {
            fn neighbours(
                map: &Heightmap,
                position: &Position,
                length: usize,
            ) -> HashMap<Position, usize> {
                let mut hashmap = HashMap::new();
                if position.x > 0
                    && &map.elevations[position.y][position.x - 1]
                        - &map.elevations[position.y][position.x]
                        <= 1
                {
                    hashmap.insert(
                        Position {
                            x: position.x - 1,
                            y: position.y,
                        },
                        length + 1,
                    );
                }
                if position.x < map.elevations[position.y].len() - 1
                    && &map.elevations[position.y][position.x + 1]
                        - &map.elevations[position.y][position.x]
                        <= 1
                {
                    hashmap.insert(
                        Position {
                            x: position.x + 1,
                            y: position.y,
                        },
                        length + 1,
                    );
                }
                if position.y > 0
                    && &map.elevations[position.y - 1][position.x]
                        - &map.elevations[position.y][position.x]
                        <= 1
                {
                    hashmap.insert(
                        Position {
                            x: position.x,
                            y: position.y - 1,
                        },
                        length + 1,
                    );
                }
                if position.y < map.elevations.len() - 1
                    && &map.elevations[position.y + 1][position.x]
                        - &map.elevations[position.y][position.x]
                        <= 1
                {
                    hashmap.insert(
                        Position {
                            x: position.x,
                            y: position.y + 1,
                        },
                        length + 1,
                    );
                }
                hashmap
            }
            let mut acc: HashMap<Position, (bool, usize)> = HashMap::new();
            acc.insert(start.clone(), (true, 0));
            let mut pos = start.clone();
            let mut length = 0;
            while !acc.keys().any(|pos| pos == &map.goal) {
                for (neighbour, length) in neighbours(map, &pos, length) {
                    let (minimized, previous) = acc.entry(neighbour).or_insert((false, usize::MAX));
                    if !*minimized && *previous > length {
                        *previous = length;
                    }
                }
                if let Some(mut next) = acc
                    .iter_mut()
                    .filter(|(_, (b, _))| !b)
                    .min_by_key(|(_, (_, w))| *w)
                {
                    next.1 .0 = true;
                    pos = next.0.clone();
                    length = next.1 .1;
                } else {
                    break;
                }
            }
            acc.iter()
                .find(|(pos, _)| pos == &&map.goal)
                .map(|e| e.1 .1)
        }
        let mut starts = vec![];
        for (y, v) in self.elevations.iter().enumerate() {
            for (x, e) in v.iter().enumerate() {
                if e.0 == 'a' {
                    starts.push(Position { x, y })
                }
            }
        }
        starts
            .par_iter()
            .flat_map(|p| consider(self, p))
            .min()
            .unwrap()
    }
}

impl TryFrom<&str> for Heightmap {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut elevations = vec![];
        let mut start = None;
        let mut goal = None;
        for (y, line) in value.lines().enumerate() {
            let mut temp = vec![];
            for (x, char) in line.chars().enumerate() {
                if char == 'S' {
                    start = Some(Position { x, y });
                } else if char == 'E' {
                    goal = Some(Position { x, y });
                }
                temp.push(Elevation::try_from(char)?);
            }
            elevations.push(temp);
        }
        let start = start.ok_or(ParseError {
            kind: "heightmap (no start)".to_string(),
            spec: value.to_string(),
        })?;
        let goal = goal.ok_or(ParseError {
            kind: "heightmap (no goal)".to_string(),
            spec: value.to_string(),
        })?;
        Ok(Heightmap {
            elevations,
            start,
            goal,
        })
    }
}

#[derive(Clone, Eq, PartialEq)]
pub enum Data {
    Integer(usize),
    List(Vec<Data>),
}

fn integer_data<Input>() -> impl Parser<Input, Output = Data>
where
    Input: Stream<Token = char>,
{
    many1(digit()).map(|string: String| Data::Integer(string.parse::<usize>().unwrap()))
}

fn list_data<Input>() -> impl Parser<Input, Output = Data>
where
    Input: Stream<Token = char>,
{
    (ch('['), sep_by(data(), ch(',')), ch(']'))
        .map(|(_, els, _): (char, Vec<Data>, char)| Data::List(els))
}

parser! {
    fn data[Input]()(Input) -> Data
    where [ Input: Stream<Token = char> ] {
        choice!(attempt(list_data()), integer_data())
    }
}

impl Data {
    pub fn decoder(value: &str) -> usize {
        let mut packets = vec![];
        for pair in value.split("\n\n") {
            let (left, right) = pair.split_once('\n').unwrap();
            let (left, _) = data().easy_parse(left).unwrap();
            let (right, _) = data().easy_parse(right).unwrap();
            packets.push(left);
            packets.push(right);
        }
        let start = Data::List(vec![Data::List(vec![Data::Integer(2)])]);
        let end = Data::List(vec![Data::List(vec![Data::Integer(6)])]);
        packets.push(start.clone());
        packets.push(end.clone());
        packets.sort();
        let mut key = 1;
        for (i, packet) in packets.iter().enumerate() {
            if packet == &start || packet == &end {
                key *= i + 1;
            }
        }
        key
    }
}

impl Ord for Data {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (Data::Integer(left), Data::Integer(right)) => left.cmp(right),
            (Data::Integer(left), Data::List(_)) => {
                Data::List(vec![Data::Integer(*left)]).cmp(other)
            }
            (Data::List(_), Data::Integer(right)) => {
                self.cmp(&Data::List(vec![Data::Integer(*right)]))
            }
            (Data::List(left), Data::List(right)) => {
                for pair in left.iter().zip(right) {
                    let cmp = pair.0.cmp(pair.1);
                    if cmp != Ordering::Equal {
                        return cmp;
                    }
                }
                left.len().cmp(&right.len())
            }
        }
    }
}

impl PartialOrd for Data {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_initialize_example() {
        for (spec, expected) in [
            ("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 19),
            ("bvwbjplbgvbhsrlpgdmjqwftvncz", 23),
            ("nppdvjthqldpwncqszvftbrmjlhg", 23),
            ("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 29),
            ("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 26),
        ] {
            let mut device = Device::from(spec);
            assert_eq!(device.initialize(), Ok(expected))
        }
    }

    #[test]
    fn test_initialize_input() {
        let mut device = Device::from(include_str!("../resources/device.txt"));
        assert_eq!(device.initialize(), Ok(2773))
    }

    #[test]
    fn test_path_example() -> anyhow::Result<()> {
        let heightmap = Heightmap::try_from(
            "Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi",
        )?;
        assert_eq!(heightmap.path(), 29);
        Ok(())
    }

    #[test]
    fn test_path_input() -> anyhow::Result<()> {
        let heightmap = Heightmap::try_from(include_str!("../resources/heightmap.txt"))?;
        assert_eq!(heightmap.path(), 478);
        Ok(())
    }

    #[test]
    fn test_decoder_example() -> anyhow::Result<()> {
        let decoder = Data::decoder(
            "[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]",
        );
        assert_eq!(decoder, 140);
        Ok(())
    }

    #[test]
    fn test_decoder_input() -> anyhow::Result<()> {
        let decoder = Data::decoder(include_str!("../resources/distress.txt"));
        assert_eq!(decoder, 22184);
        Ok(())
    }
}
