use std::cell::RefCell;
use std::rc::{Rc, Weak};

use lazy_static::lazy_static;
use regex::Regex;

use crate::ParseError;

trait Size {
    fn size(&self) -> usize;
}

struct Plain {
    #[allow(dead_code)]
    name: String,
    size: usize,
}

impl Size for Plain {
    fn size(&self) -> usize {
        self.size
    }
}

impl TryFrom<&str> for Plain {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"(\d+) (.*)").unwrap();
        }
        match RE.captures_iter(value).next() {
            Some(plain) => Ok(Plain {
                name: plain[2].to_string(),
                size: plain[1].parse::<usize>().unwrap(),
            }),
            None => Err(ParseError {
                kind: "plain file".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

struct Directory {
    name: String,
    parent: Option<Weak<RefCell<Directory>>>,
    contents: Vec<Rc<RefCell<File>>>,
}

impl Directory {
    fn find_directories_larger_than(&self, size: usize) -> Vec<Rc<RefCell<File>>> {
        self.contents
            .iter()
            .flat_map(|file| {
                let mut dirs = vec![];
                if let File::Directory(dir) = &*file.borrow() {
                    if file.borrow().size() >= size {
                        dirs.push(file.clone());
                        dirs.extend(dir.borrow().find_directories_larger_than(size));
                    }
                }
                dirs
            })
            .collect()
    }
}

impl Size for Directory {
    fn size(&self) -> usize {
        self.contents.iter().map(|file| file.borrow().size()).sum()
    }
}

impl TryFrom<&str> for Directory {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"dir (.*)").unwrap();
        }
        match RE.captures_iter(value).next() {
            Some(directory) => Ok(Directory {
                name: directory[1].to_string(),
                parent: None,
                contents: vec![],
            }),
            None => Err(ParseError {
                kind: "directory".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

enum File {
    Plain(Rc<RefCell<Plain>>),
    Directory(Rc<RefCell<Directory>>),
}

impl Size for File {
    fn size(&self) -> usize {
        match self {
            File::Plain(plain) => plain.borrow().size(),
            File::Directory(directory) => directory.borrow().size(),
        }
    }
}

impl TryFrom<&str> for File {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match Directory::try_from(value) {
            Ok(directory) => Ok(File::Directory(Rc::new(RefCell::new(directory)))),
            Err(_) => match Plain::try_from(value) {
                Ok(plain) => Ok(File::Plain(Rc::new(RefCell::new(plain)))),
                Err(_) => Err(ParseError {
                    kind: "directory entry".to_string(),
                    spec: value.to_string(),
                }),
            },
        }
    }
}

pub struct Filesystem(Rc<RefCell<Directory>>);

impl Filesystem {
    pub fn reclaimable_space(&self) -> usize {
        self.0
            .borrow()
            .find_directories_larger_than(
                30_000_000 - (Filesystem::total_size() - self.0.borrow().size()),
            )
            .iter()
            .map(|dir| dir.borrow().size())
            .min()
            .unwrap_or_default()
    }

    const fn total_size() -> usize {
        70_000_000
    }
}

impl TryFrom<&str> for Filesystem {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let root = Rc::new(RefCell::new(Directory {
            name: "/".to_string(),
            parent: None,
            contents: vec![],
        }));
        let mut cur = root.clone();

        let mut lines = value.lines().peekable();
        assert_eq!(lines.next(), Some("$ cd /"));

        while let Some(line) = lines.peek() {
            match line {
                &"$ ls" => {
                    lines.next();
                    while let Some(line) = lines.next_if(|line| !line.starts_with('$')) {
                        let mut file = File::try_from(line)?;
                        if let File::Directory(dir) = &mut file {
                            dir.borrow_mut().parent = Some(Rc::downgrade(&cur))
                        }
                        cur.borrow_mut().contents.push(Rc::new(RefCell::new(file)));
                    }
                }
                cd if cd.starts_with("$ cd ") => {
                    let name = cd.strip_prefix("$ cd ").unwrap();
                    cur = if name == ".." {
                        cur.borrow().parent.as_ref().unwrap().upgrade().unwrap()
                    } else {
                        let borrow = cur.borrow();
                        let mut files = borrow.contents.iter().cloned();
                        files
                            .find_map(|d| {
                                if let File::Directory(dir) = &*d.borrow() {
                                    (dir.borrow().name == name).then_some(dir.clone())
                                } else {
                                    None
                                }
                            })
                            .unwrap()
                            .clone()
                    };
                    lines.next();
                }
                _ => panic!("unexpected line {}", line),
            }
        }

        Ok(Filesystem(root))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_to_delete_example() -> anyhow::Result<()> {
        let filesystem = Filesystem::try_from(
            "$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k",
        )?;
        assert_eq!(filesystem.reclaimable_space(), 24933642);
        Ok(())
    }

    #[test]
    fn test_to_delete_input() -> anyhow::Result<()> {
        let filesystem = Filesystem::try_from(include_str!("../resources/filesystem.txt"))?;
        assert_eq!(filesystem.reclaimable_space(), 9847279);
        Ok(())
    }
}
