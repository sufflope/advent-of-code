use std::cmp::Ordering;
use std::num::ParseIntError;

use derive_more::{Add, Display, Sum};
use itertools::Itertools;

#[derive(Add, Clone, Copy, Debug, Display, Eq, PartialEq, Sum)]
pub struct Calories(usize);

impl Ord for Calories {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0).reverse()
    }
}

impl PartialOrd for Calories {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Default)]
pub struct Inventory(Vec<Vec<Calories>>);

impl Inventory {
    pub fn top<const N: usize>(&self) -> Calories {
        self.0
            .iter()
            .map(|item| item.iter().copied().sum())
            .k_smallest(N)
            .sum()
    }
}

impl TryFrom<&str> for Inventory {
    type Error = ParseIntError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut inventory = Inventory::default();
        let mut item = vec![];
        for line in value.lines() {
            if line.is_empty() {
                inventory.0.push(item);
                item = vec![]
            } else {
                item.push(Calories(line.parse::<usize>()?))
            }
        }
        // Account for files without an empty last line
        if !item.is_empty() {
            inventory.0.push(item);
        }
        Ok(inventory)
    }
}

#[cfg(test)]
mod tests {
    use crate::inventory::{Calories, Inventory};

    #[test]
    fn test_top_example() -> anyhow::Result<()> {
        let inventory = Inventory::try_from(
            "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000",
        )?;
        assert_eq!(inventory.top::<1>(), Calories(24000));
        assert_eq!(inventory.top::<3>(), Calories(45000));
        Ok(())
    }

    #[test]
    fn test_top_input() -> anyhow::Result<()> {
        let inventory = Inventory::try_from(include_str!("../resources/inventory.txt"))?;
        assert_eq!(inventory.top::<1>(), Calories(67633));
        assert_eq!(inventory.top::<3>(), Calories(199628));
        Ok(())
    }
}
