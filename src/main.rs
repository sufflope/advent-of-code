mod assignment;
mod cargo;
mod cave;
mod cpu;
mod device;
mod filesystem;
mod inventory;
mod monkey;
mod rope;
mod rps;
mod rucksack;
mod sensor;
mod treehouse;
mod valve;

use crate::device::Heightmap;
use assignment::Pair;
use cargo::Operator;
use cave::Cave;
use cpu::Cpu;
use device::{Data, Device};
use filesystem::Filesystem;
use inventory::Inventory;
use log::LevelFilter;
use monkey::Monkey;
use rope::Rope;
use rps::Tournament;
use rucksack::Rucksack;
use sensor::Sensor;
use treehouse::Patch;

fn main() -> anyhow::Result<()> {
    env_logger::builder().filter_level(LevelFilter::Info).init();

    let inventory = Inventory::try_from(include_str!("../resources/inventory.txt"))?;
    println!(
        "Elf carrying the most Calories carries {}",
        inventory.top::<1>()
    );
    println!(
        "The three elves carrying the most Calories carry {}",
        inventory.top::<3>()
    );

    let tournament = Tournament::try_from(include_str!("../resources/tournament.txt"))?;
    println!("Your tournament score is {}", tournament.score());

    let rucksacks = Rucksack::parse_vec(include_str!("../resources/rucksacks.txt"))?;
    println!(
        "Total value of errors in rucksacks is {}",
        Rucksack::errors(&rucksacks)
    );
    println!(
        "Total value of badges in rucksacks is {}",
        Rucksack::badges(&rucksacks)
    );

    let pairs = Pair::parse_vec(include_str!("../resources/assignments.txt"))?;
    println!(
        "Pair of assignments overlapping: {}",
        pairs.iter().filter(|pair| pair.overlaps()).count()
    );

    let mut operator = Operator::try_from(include_str!("../resources/cargo.txt"))?;
    println!("Crates on top of stack: {}", operator.operate());

    let mut device = Device::from(include_str!("../resources/device.txt"));
    println!(
        "Start-of-packet marker is at {}",
        device.initialize().unwrap()
    );

    let filesystem = Filesystem::try_from(include_str!("../resources/filesystem.txt"))?;
    println!(
        "Filesystem reclaimable space: {}",
        filesystem.reclaimable_space()
    );

    let patch = Patch::<99>::try_from(include_str!("../resources/trees.txt"))?;
    println!("Highest scenic score: {}", patch.highest_scenic_score());

    let rope_positions = Rope::route(include_str!("../resources/rope.txt"))?;
    println!("Rope tail positions: {}", rope_positions);

    let mut cpu = Cpu::default();
    println!(
        "CRT: {}",
        cpu.run(include_str!("../resources/instructions.txt"))?
    );

    let monkey_business = Monkey::business(include_str!("../resources/monkeys.txt"))?;
    println!("Monkey business: {}", monkey_business);

    let path = Heightmap::try_from(include_str!("../resources/heightmap.txt"))?.path();
    println!("Shortest path: {}", path);

    let decoder = Data::decoder(include_str!("../resources/distress.txt"));
    println!("Distress signal decoder: {}", decoder);

    let mut cave: Cave = Cave::try_from(include_str!("../resources/cave.txt"))?;
    println!("Sand count: {}", cave.fill());

    let tuning_frequency: usize =
        Sensor::tuning_frequency(include_str!("../resources/sensors.txt"), 4000000)?;
    println!("Tuning frequency: {}", tuning_frequency);

    Ok(())
}

#[derive(Debug, thiserror::Error)]
#[error("Invalid {}: '{}'", .kind, .spec)]
pub struct ParseError {
    kind: String,
    spec: String,
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
struct Position {
    x: usize,
    y: usize,
}
