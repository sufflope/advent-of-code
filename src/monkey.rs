use itertools::Itertools;
use lazy_static::lazy_static;
use regex::Regex;

use crate::ParseError;

pub struct Monkey {
    items: Vec<Item>,
    operation: Operation,
    test: Test,
    inspected: usize,
}

impl Monkey {
    pub fn business(spec: &str) -> anyhow::Result<usize> {
        let mut monkeys = spec
            .split("\n\n")
            .map(Monkey::try_from)
            .collect::<Result<Vec<_>, _>>()?;
        let lcm: usize = monkeys.iter().map(|monkey| monkey.test.dividend).product();
        for _ in 0..10000 {
            let len = monkeys.len();
            for i in 0..len {
                let items = std::mem::take(&mut monkeys[i].items);
                for mut item in items {
                    let target = monkeys[i].inspect(&mut item, lcm);
                    monkeys[target].items.push(item);
                }
            }
        }
        Ok(monkeys
            .iter()
            .map(|monkey| monkey.inspected)
            .sorted()
            .rev()
            .take(2)
            .product())
    }

    fn inspect(&mut self, item: &mut Item, lcm: usize) -> usize {
        self.inspected += 1;
        item.0 = self.operation.apply(item.0) % lcm;
        self.test.target(item.0)
    }
}

impl TryFrom<&str> for Monkey {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"Monkey \d+:\n  Starting items: (.*)").unwrap();
        }
        let monkey = value.lines().take(2).collect::<Vec<_>>();
        let operation = Operation::try_from(value.lines().skip(2).take(1).join("").as_str())?;
        let test = Test::try_from(value.lines().skip(3).take(3).join("\n").as_str())?;
        match RE.captures_iter(&monkey.join("\n")).next() {
            Some(monkey) => {
                let items = monkey[1]
                    .split(", ")
                    .map(Item::try_from)
                    .collect::<Result<Vec<_>, _>>()?;
                Ok(Monkey {
                    items,
                    operation,
                    test,
                    inspected: 0,
                })
            }
            None => Err(ParseError {
                kind: "monkey".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

struct Item(usize);

impl TryFrom<&str> for Item {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value.parse::<usize>() {
            Ok(u) => Ok(Item(u)),
            Err(_) => Err(ParseError {
                kind: "item".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

struct Operation {
    op: Op,
    val: Val,
}

impl Operation {
    fn apply(&self, worry: usize) -> usize {
        let value = match self.val {
            Val::Constant(value) => value,
            Val::Old => worry,
        };
        match self.op {
            Op::Add => worry + value,
            Op::Multiply => worry * value,
        }
    }
}

impl TryFrom<&str> for Operation {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"\s*Operation: new = old ([\+\*]) (.*)").unwrap();
        }
        match RE.captures_iter(value).next() {
            Some(operation) => {
                let op = Op::try_from(&operation[1])?;
                let val = Val::try_from(&operation[2])?;
                Ok(Operation { op, val })
            }
            None => Err(ParseError {
                kind: "operation".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

enum Op {
    Add,
    Multiply,
}

impl TryFrom<&str> for Op {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "+" => Ok(Op::Add),
            "*" => Ok(Op::Multiply),
            _ => Err(ParseError {
                kind: "op".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

enum Val {
    Constant(usize),
    Old,
}

impl TryFrom<&str> for Val {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "old" => Ok(Val::Old),
            _ => Ok(Val::Constant(value.parse::<usize>().map_err(|_| {
                ParseError {
                    kind: "val".to_string(),
                    spec: value.to_string(),
                }
            })?)),
        }
    }
}

struct Test {
    dividend: usize,
    success: usize,
    failure: usize,
}

impl Test {
    fn target(&self, worry: usize) -> usize {
        if worry % self.dividend == 0 {
            self.success
        } else {
            self.failure
        }
    }
}

impl TryFrom<&str> for Test {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"\s*Test: divisible by (\d+)\n\s*If true: throw to monkey (\d+)\n\s*If false: throw to monkey (\d+)").unwrap();
        }
        match RE.captures_iter(value).next() {
            Some(test) => {
                let dividend = test[1].parse::<usize>().unwrap();
                let success = test[2].parse::<usize>().unwrap();
                let failure = test[3].parse::<usize>().unwrap();
                Ok(Test {
                    dividend,
                    success,
                    failure,
                })
            }
            None => Err(ParseError {
                kind: "test".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_monkey_business_example() -> anyhow::Result<()> {
        let business = Monkey::business(
            "Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1",
        )?;
        assert_eq!(business, 2713310158);
        Ok(())
    }

    #[test]
    fn test_monkey_business_input() -> anyhow::Result<()> {
        let business = Monkey::business(include_str!("../resources/monkeys.txt"))?;
        assert_eq!(business, 21115867968);
        Ok(())
    }
}
