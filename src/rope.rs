use std::collections::HashSet;

use crate::ParseError;

#[derive(Clone, Copy, Default, Eq, Hash, PartialEq)]
struct Knot {
    x: isize,
    y: isize,
}

#[derive(Default)]
pub struct Rope {
    knots: [Knot; 10],
}

impl Rope {
    fn step(&mut self, direction: &Direction) {
        let head = &mut self.knots[0];
        match direction {
            Direction::Up => head.y += 1,
            Direction::Left => head.x -= 1,
            Direction::Down => head.y -= 1,
            Direction::Right => head.x += 1,
        }
        for i in 1..10 {
            let previous = &self.knots[i - 1].clone();
            let current = &mut self.knots[i];
            let x_diff = previous.x - current.x;
            let y_diff = previous.y - current.y;
            if x_diff.abs() > 1 {
                current.x += x_diff.signum();
                if y_diff != 0 {
                    current.y += y_diff.signum();
                }
            } else if y_diff.abs() > 1 {
                current.y += y_diff.signum();
                if x_diff != 0 {
                    current.x += x_diff.signum();
                }
            }
        }
    }

    pub fn route(spec: &str) -> Result<usize, ParseError> {
        let mut rope = Rope::default();
        let mut positions = HashSet::new();
        positions.insert(rope.knots[9]);
        let moves = spec
            .lines()
            .map(Move::try_from)
            .collect::<Result<Vec<_>, _>>()?;
        for m in moves {
            for _ in 0..m.length {
                rope.step(&m.direction);
                positions.insert(rope.knots[9]);
            }
        }
        Ok(positions.len())
    }
}

enum Direction {
    Up,
    Left,
    Down,
    Right,
}

impl TryFrom<&str> for Direction {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "U" => Ok(Direction::Up),
            "L" => Ok(Direction::Left),
            "D" => Ok(Direction::Down),
            "R" => Ok(Direction::Right),
            _ => Err(ParseError {
                kind: "direction".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

struct Move {
    direction: Direction,
    length: usize,
}

impl TryFrom<&str> for Move {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match &value.split(' ').collect::<Vec<_>>().as_slice() {
            [direction, length, rest @ ..] => {
                if !rest.is_empty() {
                    log::warn!("Discarding extra move spec: '{}'", rest.join(""));
                }
                let direction = Direction::try_from(*direction)?;
                let length = length.parse::<usize>().map_err(|_| ParseError {
                    kind: "move length".to_string(),
                    spec: length.to_string(),
                })?;
                Ok(Move { direction, length })
            }
            _ => Err(ParseError {
                kind: "move".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_route_small_example() -> anyhow::Result<()> {
        let count = Rope::route(
            "R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2",
        )?;
        assert_eq!(count, 1);
        Ok(())
    }

    #[test]
    fn test_route_large_example() -> anyhow::Result<()> {
        let count = Rope::route(
            "R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20",
        )?;
        assert_eq!(count, 36);
        Ok(())
    }

    #[test]
    fn test_route_input() -> anyhow::Result<()> {
        let count = Rope::route(include_str!("../resources/rope.txt"))?;
        assert_eq!(count, 2593);
        Ok(())
    }
}
