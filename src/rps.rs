use crate::ParseError;

#[derive(Clone)]
enum Move {
    Rock,
    Paper,
    Scissor,
}

impl Move {
    fn value(&self) -> u8 {
        match self {
            Move::Rock => 1,
            Move::Paper => 2,
            Move::Scissor => 3,
        }
    }
}

impl TryFrom<char> for Move {
    type Error = ParseError;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'A' => Ok(Move::Rock),
            'B' => Ok(Move::Paper),
            'C' => Ok(Move::Scissor),
            _ => Err(ParseError {
                kind: "move".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

enum Outcome {
    Win,
    Draw,
    Loss,
}

impl Outcome {
    fn value(&self) -> u8 {
        match self {
            Outcome::Win => 6,
            Outcome::Draw => 3,
            Outcome::Loss => 0,
        }
    }
}

impl TryFrom<char> for Outcome {
    type Error = ParseError;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'X' => Ok(Outcome::Loss),
            'Y' => Ok(Outcome::Draw),
            'Z' => Ok(Outcome::Win),
            _ => Err(ParseError {
                kind: "outcome".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

struct Round {
    opponent_move: Move,
    desired_outcome: Outcome,
}

impl Round {
    fn needed_move(&self) -> Move {
        match (&self.opponent_move, &self.desired_outcome) {
            (Move::Rock, Outcome::Win) => Move::Paper,
            (Move::Rock, Outcome::Loss) => Move::Scissor,
            (Move::Paper, Outcome::Win) => Move::Scissor,
            (Move::Paper, Outcome::Loss) => Move::Rock,
            (Move::Scissor, Outcome::Win) => Move::Rock,
            (Move::Scissor, Outcome::Loss) => Move::Paper,
            (m, Outcome::Draw) => m.clone(),
        }
    }

    fn score(&self) -> u8 {
        self.needed_move().value() + self.desired_outcome.value()
    }
}

impl TryFrom<&str> for Round {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match &value.chars().collect::<Vec<_>>()[..] {
            [opponent, ' ', outcome, rest @ ..] => {
                if !rest.is_empty() {
                    log::warn!("Discarding extra round spec: '{}'", String::from_iter(rest));
                }
                let opponent_move = Move::try_from(*opponent)?;
                let desired_outcome = Outcome::try_from(*outcome)?;
                Ok(Round {
                    opponent_move,
                    desired_outcome,
                })
            }
            _ => Err(ParseError {
                kind: "round".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

pub struct Tournament(Vec<Round>);

impl Tournament {
    pub fn score(&self) -> u128 {
        self.0.iter().map(|round| round.score() as u128).sum()
    }
}

impl TryFrom<&str> for Tournament {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Ok(Tournament(
            value
                .lines()
                .map(Round::try_from)
                .collect::<Result<Vec<_>, _>>()?,
        ))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_score() -> anyhow::Result<()> {
        for (spec, score) in [
            ("A X", 3),
            ("A Y", 4),
            ("A Z", 8),
            ("B X", 1),
            ("B Y", 5),
            ("B Z", 9),
            ("C X", 2),
            ("C Y", 6),
            ("C Z", 7),
        ] {
            let round = Round::try_from(spec)?;
            assert_eq!(round.score(), score, "Invalid score for '{}'", spec)
        }
        Ok(())
    }

    #[test]
    fn test_score_example() -> anyhow::Result<()> {
        let tournament = Tournament::try_from(
            "A Y
B X
C Z",
        )?;
        assert_eq!(tournament.score(), 12);
        Ok(())
    }

    #[test]
    fn test_score_input() -> anyhow::Result<()> {
        let tournament = Tournament::try_from(include_str!("../resources/tournament.txt"))?;
        assert_eq!(tournament.score(), 16098);
        Ok(())
    }
}
