use itertools::Itertools;

use crate::ParseError;

#[derive(Clone, Copy, Eq, PartialEq)]
pub struct Item(char);

impl Item {
    pub fn value(&self) -> u8 {
        if ('a'..='z').contains(&self.0) {
            self.0 as u8 - b'a' + 1
        } else {
            self.0 as u8 - b'A' + 27
        }
    }
}

impl TryFrom<char> for Item {
    type Error = ParseError;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        if ('a'..='z').contains(&value) || ('A'..='Z').contains(&value) {
            Ok(Item(value))
        } else {
            Err(ParseError {
                kind: "item".to_string(),
                spec: value.to_string(),
            })
        }
    }
}

pub struct Rucksack(Vec<Item>, Vec<Item>);

impl Rucksack {
    pub fn parse_vec(value: &str) -> Result<Vec<Rucksack>, ParseError> {
        let rucksacks = value
            .lines()
            .map(Rucksack::try_from)
            .collect::<Result<Vec<_>, _>>()?;
        if rucksacks.len() % 3 == 0 {
            Ok(rucksacks)
        } else {
            Err(ParseError {
                kind: "rucksacks".to_string(),
                spec: value.to_string(),
            })
        }
    }

    pub fn badges(rucksacks: &[Rucksack]) -> usize {
        let mut result = 0;
        for chunk in &rucksacks.iter().chunks(3) {
            let mut group = chunk.into_iter().collect::<Vec<_>>();
            let first = group.pop().unwrap();
            let badge = first
                .items()
                .find(|item| group.iter().all(|rucksack| rucksack.items().contains(item)))
                .unwrap();
            result += badge.value() as usize;
        }
        result
    }

    pub fn errors(rucksacks: &[Rucksack]) -> usize {
        rucksacks
            .iter()
            .map(|rucksack| rucksack.find_error().value() as usize)
            .sum()
    }

    fn items(&self) -> impl Iterator<Item = &Item> {
        self.0.iter().chain(&self.1)
    }

    fn find_error(&self) -> Item {
        *self.0.iter().find(|item| self.1.contains(item)).unwrap()
    }
}

impl TryFrom<&str> for Rucksack {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        fn parse_compartment(value: &str) -> Result<Vec<Item>, ParseError> {
            value.chars().map(Item::try_from).collect::<Result<_, _>>()
        }
        if value.len() % 2 == 0 {
            let (first, second) = value.split_at(value.len() / 2);
            Ok(Rucksack(
                parse_compartment(first)?,
                parse_compartment(second)?,
            ))
        } else {
            Err(ParseError {
                kind: "rucksack".to_string(),
                spec: value.to_string(),
            })
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() -> anyhow::Result<()> {
        let rucksacks = Rucksack::parse_vec(
            "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw",
        )?;
        assert_eq!(Rucksack::errors(&rucksacks), 157);
        assert_eq!(Rucksack::badges(&rucksacks), 70);
        Ok(())
    }

    #[test]
    fn test_errors_input() -> anyhow::Result<()> {
        let rucksacks = Rucksack::parse_vec(include_str!("../resources/rucksacks.txt"))?;
        assert_eq!(Rucksack::errors(&rucksacks), 7850);
        assert_eq!(Rucksack::badges(&rucksacks), 2581);
        Ok(())
    }
}
