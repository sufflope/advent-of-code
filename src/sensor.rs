use lazy_static::lazy_static;
use regex::Regex;

use crate::ParseError;

#[derive(Clone, PartialEq)]
struct Position {
    x: isize,
    y: isize,
}

impl Position {
    fn distance(&self, other: &Position) -> usize {
        self.x.abs_diff(other.x) + self.y.abs_diff(other.y)
    }
}

pub struct Sensor {
    position: Position,
    beacon: Position,
}

impl Sensor {
    fn range(&self) -> usize {
        self.position.distance(&self.beacon)
    }

    pub fn tuning_frequency(value: &str, max: usize) -> Result<usize, ParseError> {
        let sensors = value
            .lines()
            .map(Sensor::try_from)
            .collect::<Result<Vec<_>, _>>()?;

        let elligible = |position: &Position| {
            (0..=max as isize).contains(&position.x) && (0..=max as isize).contains(&position.y)
        };

        for sensor in &sensors {
            let range = sensor.range() as isize;
            let mut position = sensor.position.clone();
            position.x -= range + 1;
            while position.x <= sensor.position.x {
                if elligible(&position)
                    && !sensors
                        .iter()
                        .any(|sensor| sensor.position.distance(&position) <= sensor.range())
                {
                    return Ok((position.x * 4000000 + position.y) as usize);
                }
                position.x += 1;
                position.y += 1;
            }
            while position.x <= sensor.position.x + range + 1 {
                if elligible(&position)
                    && !sensors
                        .iter()
                        .any(|sensor| sensor.position.distance(&position) <= sensor.range())
                {
                    return Ok((position.x * 4000000 + position.y) as usize);
                }
                position.x += 1;
                position.y -= 1;
            }
            while position.x >= sensor.position.x {
                if elligible(&position)
                    && !sensors
                        .iter()
                        .any(|sensor| sensor.position.distance(&position) <= sensor.range())
                {
                    return Ok((position.x * 4000000 + position.y) as usize);
                }
                position.x -= 1;
                position.y -= 1;
            }
            while position.x >= sensor.position.x - range - 1 {
                if elligible(&position)
                    && !sensors
                        .iter()
                        .any(|sensor| sensor.position.distance(&position) <= sensor.range())
                {
                    return Ok((position.x * 4000000 + position.y) as usize);
                }
                position.x -= 1;
                position.y += 1;
            }
        }
        Err(ParseError {
            kind: "sensor map".to_string(),
            spec: value.to_string(),
        })
    }
}

impl TryFrom<&str> for Sensor {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"Sensor at x=(-{0,1}\d+), y=(-{0,1}\d+): closest beacon is at x=(-{0,1}\d+), y=(-{0,1}\d+)").unwrap();
        }
        match RE.captures_iter(value).next() {
            Some(result) => {
                let sensor_x = result[1].parse::<isize>().unwrap();
                let sensor_y = result[2].parse::<isize>().unwrap();
                let beacon_x = result[3].parse::<isize>().unwrap();
                let beacon_y = result[4].parse::<isize>().unwrap();
                Ok(Sensor {
                    position: Position {
                        x: sensor_x,
                        y: sensor_y,
                    },
                    beacon: Position {
                        x: beacon_x,
                        y: beacon_y,
                    },
                })
            }
            None => Err(ParseError {
                kind: "sensor".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sensor_example() -> anyhow::Result<()> {
        let tuning_frequency = Sensor::tuning_frequency(
            "Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3
",
            20,
        )?;
        assert_eq!(tuning_frequency, 56000011);
        Ok(())
    }

    #[test]
    fn test_sensor_input() -> anyhow::Result<()> {
        let tuning_frequency =
            Sensor::tuning_frequency(include_str!("../resources/sensors.txt"), 4000000)?;
        assert_eq!(tuning_frequency, 13081194638237);
        Ok(())
    }
}
