use itertools::Itertools;

use crate::ParseError;

#[derive(PartialEq, PartialOrd)]
struct Tree(usize);

impl TryFrom<char> for Tree {
    type Error = ParseError;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value.to_digit(10) {
            Some(size) => Ok(Tree(size as usize)),
            None => Err(ParseError {
                kind: "tree".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

pub struct Patch<const N: usize>([[Tree; N]; N]);

impl<const N: usize> Patch<N> {
    fn get(&self, position: (usize, usize)) -> &Tree {
        &self.0[position.0][position.1]
    }

    #[allow(clippy::type_complexity)]
    fn count_visible_trees(
        &self,
        mut position: (usize, usize),
        change: fn((usize, usize)) -> Option<(usize, usize)>,
    ) -> usize {
        let mut count = 0;
        let tree = self.get(position);
        while let Some(new_position) = change(position) {
            count += 1;
            if self.get(new_position) >= tree {
                break;
            }
            position = new_position
        }
        count
    }

    fn scenic_score(&self, position: (usize, usize)) -> usize {
        #[allow(clippy::unnecessary_lazy_evaluations)]
        [
            |(x, y)| (x < N - 1).then(|| (x + 1, y)),
            |(x, y)| (x > 0).then(|| (x - 1, y)),
            |(x, y)| (y < N - 1).then(|| (x, y + 1)),
            |(x, y)| (y > 0).then(|| (x, y - 1)),
        ]
        .map(|f| self.count_visible_trees(position, f))
        .iter()
        .product()
    }

    pub fn highest_scenic_score(&self) -> usize {
        (0..N)
            .cartesian_product(0..N)
            .map(|position| self.scenic_score(position))
            .max()
            .unwrap_or_default()
    }
}

impl<const N: usize> TryFrom<&str> for Patch<N> {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let trees = value
            .lines()
            .map(|line| {
                line.chars()
                    .map(Tree::try_from)
                    .collect::<Result<Vec<_>, _>>()
                    .and_then(|trees| {
                        trees.try_into().map_err(|_| ParseError {
                            kind: "patch".to_string(),
                            spec: value.to_string(),
                        })
                    })
            })
            .collect::<Result<Vec<_>, _>>()?;
        Ok(Patch(trees.try_into().map_err(|_| ParseError {
            kind: "patch".to_string(),
            spec: value.to_string(),
        })?))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_scenic_score_example() -> anyhow::Result<()> {
        let patch: Patch<5> = Patch::try_from(
            "30373
25512
65332
33549
35390",
        )?;
        assert_eq!(patch.highest_scenic_score(), 8);
        Ok(())
    }

    #[test]
    fn test_scenic_score_input() -> anyhow::Result<()> {
        let patch: Patch<99> = Patch::try_from(include_str!("../resources/trees.txt"))?;
        assert_eq!(patch.highest_scenic_score(), 671160);
        Ok(())
    }
}
