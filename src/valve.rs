use std::collections::HashMap;

use lazy_static::lazy_static;
use regex::Regex;

use crate::ParseError;

#[derive(Clone)]
struct Valves(HashMap<String, Valve>);

impl Valves {
    fn max_flow(&self) -> usize {
        fn rec(valves: &mut Valves, at: &str, remaining: usize) -> usize {
            if remaining == 0 {
                0
            } else {
                fn n(valves: &mut Valves, next: &[String], remaining: usize) -> usize {
                        next.iter().map(|name| {
                            rec(valves, name, remaining)
                        })
                        .max()
                        .unwrap_or_default()
                }
                let next = valves.0.get(at).unwrap().next.iter().cloned().filter(|name| !valves.0.get(name.as_str()).unwrap().open).collect::<Vec<_>>();
                //println!("next = {:?}, remaining = {}", next, remaining);
                *[
                    if remaining >= 2 {
                        let mut valves = valves.clone();
                        let mut valve = valves.0.get_mut(at).unwrap();
                        valve.open = true;
                        let flow = valve.flow * (remaining - 1);
                        n(&mut valves, &next, remaining - 2) + flow
                    } else {0},
                    if remaining >= 1 {n(valves, &next, remaining-1)} else {0},
                ]
                    .iter()
                .max()
                .unwrap()
            }
        }
        rec(&mut self.clone(), "AA", 30)
    }
}

impl TryFrom<&str> for Valves {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut valves = HashMap::new();
        for valve in value
            .lines()
            .map(Valve::try_from)
            .collect::<Result<Vec<_>, _>>()?
        {
            valves.insert(valve.name.clone(), valve);
        }
        Ok(Valves(valves))
    }
}

#[derive(Clone)]
struct Valve {
    name: String,
    flow: usize,
    open: bool,
    next: Vec<String>,
}

impl TryFrom<&str> for Valve {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        lazy_static! {
            static ref RE: Regex =
                Regex::new(r"Valve (\w{2}) has flow rate=(\d+); tunnels? leads? to valves? (.*)")
                    .unwrap();
        }
        match RE.captures_iter(value).next() {
            Some(valve) => Ok(Valve {
                name: valve[1].to_string(),
                flow: valve[2].parse::<usize>().unwrap(),
                open: false,
                next: valve[3].split(", ").map(|s| s.to_string()).collect(),
            }),
            None => Err(ParseError {
                kind: "valve".to_string(),
                spec: value.to_string(),
            }),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_valves_example() -> anyhow::Result<()> {
        let valves = Valves::try_from(
            "Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
Valve BB has flow rate=13; tunnels lead to valves CC, AA
Valve CC has flow rate=2; tunnels lead to valves DD, BB
Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
Valve EE has flow rate=3; tunnels lead to valves FF, DD
Valve FF has flow rate=0; tunnels lead to valves EE, GG
Valve GG has flow rate=0; tunnels lead to valves FF, HH
Valve HH has flow rate=22; tunnel leads to valve GG
Valve II has flow rate=0; tunnels lead to valves AA, JJ
Valve JJ has flow rate=21; tunnel leads to valve II
",
        )?;
        assert_eq!(valves.max_flow(), 1651);
        Ok(())
    }
}
